# ---------------------------------------------------------------------------------------------------------------------
# REQUIRED PARAMETERS
# ---------------------------------------------------------------------------------------------------------------------

variable "prefix" {
  description = "The Azure resource prefix"
}

variable "subscription_id" {
  description = "The Azure subscription ID"
}

variable "tenant_id" {
  description = "The Azure tenant ID"
}

variable "client_id" {
  description = "The Azure client ID"
}

variable "client_secret" {
  description = "The Azure secret access key"
}

variable "location" {
  description = "The Azure Region"
}

variable "admin_username" {
  description = "The ssh user name"
}

variable "admin_password" {
  description = "The ssh user password"
}
variable "resource_group_name" {
  description = "The resource group name"
}
